# Devops API App

## install the node packages for the api tier:

```sh
→ npm install
```

## start the app

```sh
→ npm start
```

## NOTE this app uses two env variables:

- PORT: the listening PORT
- DB: Name of the database to connect
- DBUSER: Database user
- DBPASS: DB user password,
- DBHOST: Database hostname,
- DBPORT: Database server listening port

These variables need to be set

## Creating a new release docker container
To build a new release container for api a tag in the format `api-0.0.x` needs to be pushed to the repo \