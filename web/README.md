# Devops Web App


### install the node packages for the web tier:
```sh
→ npm install
```
### start the app
```sh
→ npm start
```

###  NOTE this app uses two env variables:

- PORT: the listening PORT
- API_HOST: the full url to call the API app

These two variables need to be set 

## Creating a new release docker container
To build a new release container for api a tag in the format `web-0.0.x` needs to be pushed to the repo 