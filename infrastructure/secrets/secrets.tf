resource "kubernetes_secret" "cloudflare" {
  metadata {
    name = "cloudflare-secret"
  }

  data = {
    cloudflare_api_key   = var.cf_api_key
    cloudflare_api_email = var.cf_api_email
    cloudflare_api_token = var.cf_api_token
  }

  type = "Opaque"
}

resource "kubernetes_secret" "postgres" {
  metadata {
    name = "postgres-secret"
  }

  data = {
    DBUSER = "${var.db_user}@${var.server_name}"
    DBPASS = var.db_password
    DBHOST = var.db_fqdn
  }

  type = "Opaque"
}
locals {
  namespaces_for_secret = [
    "default","flux-system"
  ]
}
resource "kubernetes_secret" "docker" {
  for_each = toset(local.namespaces_for_secret)
  metadata {
    name = "docker-cfg-${each.value}"
    namespace = each.value
  }

  type = "kubernetes.io/dockerconfigjson"

  data = {
    ".dockerconfigjson" = jsonencode({
      auths = {
        "${var.registry_server}" = {
          "username" = var.registry_username
          "password" = var.registry_password
          "email"    = var.registry_email
          "auth"     = base64encode("${var.registry_username}:${var.registry_password}")
        }
      }
    })
  }
}
