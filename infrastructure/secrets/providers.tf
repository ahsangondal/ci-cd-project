provider "kubernetes" {
  host                   = var.kube_config.0.host
  username               = var.kube_config.0.username
  password               = var.kube_config.0.password
  client_certificate     = base64decode(var.kube_config.0.client_certificate)
  client_key             = base64decode(var.kube_config.0.client_key)
  cluster_ca_certificate = base64decode(var.kube_config.0.cluster_ca_certificate)
}