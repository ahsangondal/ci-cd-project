terraform {
  required_providers {
    azurerm = {
      version = ">=2.87.0"
    }
    null = {
      version = ">=3.1.0"
    }
    kubernetes = {
      version = ">=2.6.1"
    }
    azuread = {
      source  = "hashicorp/azuread"
      version = ">=2.22.0"
    }

  }
}
