resource "azurerm_kubernetes_cluster" "aks_cluster" {
  name                    = var.cluster_name
  location                = var.resource_group_location
  resource_group_name     = var.resource_group_name
  dns_prefix              = var.cluster_name
  private_cluster_enabled = false
  default_node_pool {
    name           = "agentpool"
    node_count     = 1
    vm_size        = "Standard_B4ms"
    vnet_subnet_id = var.vnet_subnet_id
  }

  network_profile {
    network_plugin     = "kubenet"
    docker_bridge_cidr = "192.167.0.1/16"
    dns_service_ip     = "192.168.1.1"
    service_cidr       = "192.168.0.0/16"
    pod_cidr           = "172.16.0.0/22"
  }
  addon_profile {
    azure_policy { enabled = true }
    oms_agent {
      enabled                    = true
      log_analytics_workspace_id = var.log_analytics_workspace_id
    }
  }

  linux_profile {
    admin_username = "sysadmin"
    ssh_key {
      key_data = var.ssh_key
    }
  }

  identity {
    type = "SystemAssigned"
  }

  role_based_access_control {
    enabled = true
  }

  tags = var.resource_group_tags
}
