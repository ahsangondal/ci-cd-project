variable "resource_group_name" {
}
variable "resource_group_tags" {
}
variable "resource_group_location" {
}
variable "cluster_name" {
}
variable "vnet_subnet_id" {
}
variable "ssh_key" {
}
variable "log_analytics_workspace_id" {
}
