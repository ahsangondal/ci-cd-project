
## Pre-requisites

- Terraform
- Azure Cli
- Valid Azure Subscription

## Steps to run

- Login to azure via azure cli

    ` az login `

- Get the azure storage account access key from azure portal using [this guide](https://docs.microsoft.com/en-us/azure/storage/common/storage-account-keys-manage?tabs=azure-portal#view-account-access-keys) and run

    ` export ARM_ACCESS_KEY=<access-key> `
- Initialize terraform

    ` terraform init `

- Apply/Plan terraform

    ` terraform apply `
