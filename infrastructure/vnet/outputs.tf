output "subnet_id" {
  value = azurerm_subnet.subnet.id
}
output "postgres_subnet_id" {
  value = azurerm_subnet.postgres_subnet.id
}
output "vnet_id" {
  value = azurerm_virtual_network.vnet.id
}
