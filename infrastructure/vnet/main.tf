terraform {
  required_providers {
    azurerm = {
      version = "2.87.0"
    }
    azuread = {
      source  = "hashicorp/azuread"
      version = "2.22.0"
    }
  }
}
