variable "resource_group_name" {
}
variable "vnet_name" {
  default = "staging"
}
variable "vnet_address_space" {
  default = ["10.2.0.0/16"]
}

variable "subnet_address_space" {
  default = ["10.2.0.0/24"]
}
variable "postgres_subnet_address_space" {
  default = ["10.2.1.0/24"]
}
variable "resource_group_location" {
}
