resource "azurerm_resource_group" "default_group" {
  name     = var.resource_group_name
  location = var.resource_group_location

  tags = var.resource_group_tags
}

resource "azurerm_log_analytics_workspace" "insights" {
  name                = "logging-${var.resource_group_name}"
  location            = azurerm_resource_group.default_group.location
  resource_group_name = azurerm_resource_group.default_group.name
  retention_in_days   = 30
}

module "aks" {
  source                     = "./aks"
  resource_group_name        = azurerm_resource_group.default_group.name
  resource_group_location    = azurerm_resource_group.default_group.location
  resource_group_tags        = var.resource_group_tags
  cluster_name               = var.cluster_name
  vnet_subnet_id             = module.vnet.subnet_id
  ssh_key                    = var.default_ssh_key
  log_analytics_workspace_id = azurerm_log_analytics_workspace.insights.id
}

module "vnet" {
  source                  = "./vnet"
  resource_group_name     = azurerm_resource_group.default_group.name
  resource_group_location = azurerm_resource_group.default_group.location
}

module "flux" {
  source          = "./flux"
  kube_config     = module.aks.kube_config
  gitlab_owner    = var.gitlab_owner
  gitlab_token    = var.gitlab_token
  repository_name = var.repository_name
  registry_username = var.registry_username
}

module "postgresql" {
  source                     = "./postgres"
  resource_group_name        = azurerm_resource_group.default_group.name
  resource_group_location    = azurerm_resource_group.default_group.location
  server_name                = var.server_name
  subnet_id                  = module.vnet.subnet_id
  vnet_id                    = module.vnet.vnet_id
  db_user                    = var.db_user
  db_password                = var.db_password
  log_analytics_workspace_id = azurerm_log_analytics_workspace.insights.id
}

module "secrets" {
  source            = "./secrets"
  kube_config       = module.aks.kube_config
  cf_api_key        = var.cf_api_key
  cf_api_email      = var.cf_api_email
  cf_api_token      = var.cf_api_token
  registry_email    = var.registry_email
  registry_username = var.registry_username
  registry_password = module.flux.container_registry_deploy_token
  registry_server   = var.registry_server
  db_user           = var.db_user
  db_password       = var.db_password
  db_fqdn           = module.postgresql.fqdn
  server_name       = var.server_name
}
