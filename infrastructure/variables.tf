variable "resource_group_name" {
  default = "staging-rg"
}

variable "resource_group_tags" {
  default = {
    environment = "staging"
  }
}

variable "resource_group_location" {
  default = "East US"
}

variable "cluster_name" {
  default = "staging1-aks"
}
variable "default_ssh_key" {
}

variable "gitlab_owner" {
  description = "gitlab owner"
  type        = string
}

variable "gitlab_token" {
  description = "gitlab token"
  type        = string
  sensitive   = true
}

variable "repository_name" {
  description = "gitlab repository name"
  type        = string
  default     = "toptal-test"
}
variable "server_name" {
  description = "postgres server name"
  type        = string
  default     = "db-test-ahsangondal"
}

variable "cf_api_key" {}
variable "cf_api_token" {}
variable "cf_api_email" {}
variable "registry_email" {}
variable "registry_username" {}
variable "registry_password" {}
variable "registry_server" {}
variable "db_user" {}
variable "db_password" {}