provider "kubernetes" {
  host                   = var.kube_config.0.host
  username               = var.kube_config.0.username
  password               = var.kube_config.0.password
  client_certificate     = base64decode(var.kube_config.0.client_certificate)
  client_key             = base64decode(var.kube_config.0.client_key)
  cluster_ca_certificate = base64decode(var.kube_config.0.cluster_ca_certificate)
}

provider "kubectl" {
  load_config_file       = false
  host                   = var.kube_config.0.host
  username               = var.kube_config.0.username
  password               = var.kube_config.0.password
  client_certificate     = base64decode(var.kube_config.0.client_certificate)
  client_key             = base64decode(var.kube_config.0.client_key)
  cluster_ca_certificate = base64decode(var.kube_config.0.cluster_ca_certificate)
}

provider "gitlab" {
  token = var.gitlab_token
}

# SSH
locals {
  known_hosts = "gitlab.com ecdsa-sha2-nistp256 AAAAE2VjZHNhLXNoYTItbmlzdHAyNTYAAAAIbmlzdHAyNTYAAABBBFSMqzJeV9rUzU4kWitGjeR4PWSa29SPqJ1fVkhtj3Hw9xjLVXVYrU9QlYWrOLXBpQ6KWjbjTDTdDkoohFzgbEY="
}
