variable "kube_config" {
}
variable "gitlab_owner" {
}

variable "gitlab_token" {
}

variable "repository_name" {
}

variable "registry_username" {
}

variable "repository_visibility" {
  description = "how visible is the gitlab repo"
  type        = string
  default     = "private"
}

variable "branch" {
  description = "branch name"
  type        = string
  default     = "master"
}

variable "target_path" {
  description = "flux sync target path"
  type        = string
  default     = "deployments"
}