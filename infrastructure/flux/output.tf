output "container_registry_deploy_token" {
  value = gitlab_deploy_token.container_registry.token 
  sensitive = true
}