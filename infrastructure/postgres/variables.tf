variable "subnet_id" {}
variable "vnet_id" {}
variable "server_name" {}
variable "resource_group_location" {}
variable "resource_group_name" {}
variable "db_user" {}
variable "db_password" {}
variable "log_analytics_workspace_id" {}