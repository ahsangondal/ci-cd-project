resource "azurerm_postgresql_server" "server" {
  name                = var.server_name
  location            = var.resource_group_location
  resource_group_name = var.resource_group_name

  sku_name = "B_Gen5_1"

  storage_mb            = 5120
  backup_retention_days = 7


  administrator_login          = var.db_user
  administrator_login_password = var.db_password
  version                      = "11"
  ssl_enforcement_enabled      = false
}

resource "azurerm_monitor_diagnostic_setting" "server" {
  name               = "server"
  target_resource_id = azurerm_postgresql_server.server.id
  log_analytics_workspace_id = var.log_analytics_workspace_id

  log {
    category = "PostgreSQLLogs"
    enabled  = true

    retention_policy {
      enabled = false
    }
  }

  metric {
    category = "AllMetrics"

    retention_policy {
      enabled = true
      days = 1
    }
  }
}
# resource "azurerm_postgresql_virtual_network_rule" "vnet_rules" {
#   name                                 = "aks-vnet-rule"
#   resource_group_name                  = var.resource_group_name
#   server_name                          = azurerm_postgresql_server.server.name
#   subnet_id                            = var.subnet_id
#   ignore_missing_vnet_service_endpoint = true

# }
resource "azurerm_postgresql_firewall_rule" "allow" {
  name                = "allowAzure"
  resource_group_name = var.resource_group_name
  server_name         = azurerm_postgresql_server.server.name
  start_ip_address    = "0.0.0.0"
  end_ip_address      = "0.0.0.0"
}