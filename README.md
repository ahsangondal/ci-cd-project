
# Sample 3tier app

## Implementation Presentation
[View Presentation](https://docs.google.com/presentation/d/1aHs2hoQhSrVwfKDz8P7q48FqPmhZPUTpKKIOV-dU72w/edit?usp=sharing)

[Api link](https://api.devplusops.co)

[Web link](https://web.devplusops.co)

This repo contains code for a Node.js multi-tier application.

The application overview is as follows

```
web <=> api <=> db
```

The folders `web` and `api` respectively describe how to install and run each app.

# Infrastructure
The folder `infrastructure` contains terraform code that is used to provision the infrastructure and components needed to run the app leveraging:

- Azure Managed PostgreSQL database for the db
- A kubernetes cluster to run the node apps using Azure Kubernetes Service
- Flux CD to setup continuous delivery of the app and 
- Creating gitlab deploy keys and tokens for use with container registry for app images
- Create secrets needed for the cluster, docker imagepull secrets, postgres db connection secrets and cloudflare secret containing token and api_key

# Deployments
The folder `deployments` contains fluxcd deployments on the principles of GitOps. Flux CD listens to changes on the directory and applies all the changes itself on the cluster

- Deploy nginx-ingress to serve as a proxy
- Deploy external-dns to automatically create dns records in cloudflare
- Deploy prometheus stack to monitor the kubernetes cluster including grafana to allow easy visualisation
- Deploy the app and web with config set to rolling update
- Deploy flux auto image update flow, which scans the container registry for newer semver tags and keeps the app and web images up to date with the latest release

# Terraform configuration

The project uses the http backend provided by gitlab to run terraform projects. The gitlab ci/cd pipeline is set to pickup code in the `infrastructure` repo and run terraform deployments on any changes to the folder. The configuration can be found in `.gitlab-ci.yml` in the root folder.

# Continuous Integration and Container Registry

The project is setup to leverage gitlab ci/cd pipelines and gitlab container registry provided out of the box with the project. Please ensure the features are enabled in the project settings. \
CI is setup in `.gitlab-ci.yml`, which triggers a docker build and push run for any changes in the api and web code directories or if a tag is pushed in the release semver format. \

To build a new release container for api a tag in the format `api-0.0.x` needs to be pushed. \
To build a new release container for web a tag in the format `api-0.0.x` needs to be pushed. \



# Pre-Requisites
The ci/cd repo/project needs to have the following variables required to be set

### Azure Environment Variables
`ARM_CLIENT_ID` : Azure Client ID \
`ARM_CLIENT_SECRET` : Azure Client Secret \
`ARM_SUBSCRIPTION_ID` : Azure Subscription Id \
`ARM_TENANT_ID` : Azure Tenant Id


### Terrform Input Variables:
`TF_VAR_cf_api_email`: email address for the cloudflare account hosting the dns \
`TF_VAR_cf_api_key`: api key for the cloudflare account hosting the dns \
`TF_VAR_cf_api_token`: api token for the cloudflare account hosting the dns \
`TF_VAR_db_password`: db password to set when creating the postgres instance \
`TF_VAR_db_user`: db username to set when creating the postgres instance \
`TF_VAR_default_ssh_key`: default ssh public key to be adding to the kubernetes cluster nodes to enable ssh access \
`TF_VAR_gitlab_owner` : gitlab project owner e.g toptal14 \
`TF_VAR_gitlab_token` : gitlab personal access token with code push priveleages. This is essential to create new deploy tokens,keys etc \
`TF_VAR_registry_email` : gitlab container registery email which was used to create the project hosting the registry \
`TF_VAR_registry_server` : container registry server e.g. registry.gitlab.com
